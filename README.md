# Playbooks Ansible

Some Ansible Playbooks:

*  **add_authkeys**: Add SSH public keys to authorized_keys files of root user.
*  **add_epel**: Add EPEL repo. For CentOS servers.
*  **addtoforeman**: Add CentOS machine to Foreman server. Install Puppet Agent. 
*  **install_apache**: Install and enables Apache webserver. For CentOS servers.
*  **install_docker**: Install and enables Docker service. For CentOS servers.
*  **install_docker_compose**: Install Docker Compose. For CentOS servers.
*  **install_jenkins**: Install and enables Jenkins server. For CentOS servers.
*  **install_mariadb**: Install and enable MariaDB server (distro version). Runs mysql_secure_installation. For CentOS servers.
*  **install_mariadb_103_c7**: Install and enable MariaDB 10.3 server on CentOS 7. Runs mysql_sccure_installation."
*  **install_nginx**: Install and enables Nginx webserver. For CentOS servers. Requires install_epel playbook.
*  **install_ntpd**: Install and enabled NTP service. For CentOS servers.
*  **install_rancher**: Install and enables Rancher Server. For CentOS servers. Requires install_docker playbook.
*  **install_vmwaretools**: Install and enables VMWareTools. For CentOS and Debian/Ubuntu servers.
*  **install_zabbixagent_c7**: Install Zabbix Agent 3.4 with PSK enabled on CentOS 7 machine.
*  **mariadb_createdb**: Creates new MariaDB database.
*  **max2kernels**: Delete old kernels. Change retention of old kernels to 2 (old value: 5). For CentOS servers.
*  **nginx_reverseproxy_ssl**: Creates a reverse proxy on Nginx, using HTTPS with a existing SSL certificate. For CentOS servers.
*  **nginx_vhost_selfssl**: Create a new vhosts in Nginx, with a new selfsigned certificate. For CentOS servers. Requires selfcerts playbook.
*  **php_remi_c7**: Add Remi's repo and installs PHP 7.3 on CentOS 7 server.
*  **selfcerts**: Generates self-signed certificates.
*  **selinux_change**: Changes SELinux context (no reboot). For CentOS servers.
*  **upgrade_nextcloud**: Upgrade existing Nextcloud installation. For CentOS servers.
*  **uptodate**: Install OS updates (no reboot). For CentOS and Debian/Ubuntu servers.
